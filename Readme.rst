===============
Vim Compilation
===============

In order to be successfully used there needs to be support for

  * Python
  * Ruby
  * X11 for clipboard
  * Unicode

To have that we should compile vim from source through the following commands::

     $ hg clone https://vim.googlecode.com/hg/ vim
     $ ./configure --with-features=big --enable-gui --with-x --enable-pythoninterp --enable-rubyinterp
     $ make && sudo make install

