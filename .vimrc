set nocompatible 
filetype indent plugin on
syntax on
set wildmenu wildmode=full
let mapleader = ","
"set showcmd
set ignorecase hlsearch incsearch
set backspace=indent,eol,start
set nostartofline
set ruler
set laststatus=2
set mouse=a
set cmdheight=1
set shiftwidth=4 softtabstop=4  " tab column size
" Press Space to turn off search highlighting and clear any message.
":nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>
" Silent.
" Allow switching from an unsaved buffer without saving it first.
set hidden
if has('gui_running')
  set guioptions-=T  " no toolbar
endif
let &t_Co=256
let &t_AF="\e[38;5;%dm"
let &t_AB="\e[48;5;%dm"

"colorscheme murphy  " choose your own

" ~/.vimrc (configuration file for vim only)
" skeletons

autocmd BufNewFile      *.spec  call SKEL_spec()
" filetypes
filetype plugin on
filetype indent on
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set number
"let g:JavaImpPaths = "..."
"let g:JavaImpDataDir = "..."
" Automatic tag closing for html and xml (closetag plugin)
"autocmd Filetype html,xhtml,xml,xsl source ~/scripts/closetag.vim
autocmd BufEnter * :syntax sync fromstart
"let g:closetag_html_style=1
"source ~/.vim/scripts/closetag.vim

"Syntax highlighting lines
filetype plugin indent on
syntax enable

set autoread

set guifont=Lucida_Console:h9:cANSI
"backspace fix
set backspace=indent,eol,start
"set lines=50 columns=100
set clipboard=unnamed
"fix the blinking
autocmd VimEnter * set vb t_vb=
autocmd FileType java set tags=~/.tags
set complete=.,w,b,u,t,i
"tagbar plugin map
nmap ,tt :TagbarToggle<CR> 
"eclim javasearching
map ,jc :JavaSearchContext<CR>  
"ignores for command-t
set wildignore+=*.class,.git,.hg,.svn,target/**
" EasyGrep
let g:EasyGrepMode=2
let g:EasyGrepCommand=0
let g:EasyGrepRecursive=1
let g:EasyGrepIgnoreCase=1
map <F2> <Esc>:NERDTreeToggle<CR> "Toggle the file browser
map <F3> <Esc>:TlistToggle<CR>
map <F4> <Esc>:NERDTreeFind<CR> "Find the current file in the file browser
"Open file under cursor
map <C-i> :call OpenVariableUnderCursor(expand("<cword>"))<CR>
map <Leader>h :call FindSubClasses(expand("<cword>"))<CR>

function! OpenVariableUnderCursor(varName)
    let filename = substitute(a:varName,'(<w+>)', 'u1', 'g')
    :call OpenFileUnderCursor(filename)
endfunction

function! OpenFileUnderCursor(filename)
   let ext = fnamemodify(expand("%:p"), ":t:e")
   execute ":find " . a:filename . "." . ext
endfunction

function! FindSubClasses(filename)
    execute ":Grep \(implements\|extends\) " . a:filename
endfunction
" ~/.vimrc ends here
